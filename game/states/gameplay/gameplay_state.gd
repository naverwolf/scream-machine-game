extends State
# State which handles general gameplay.


var current_room: Room setget ,_get_current_room

onready var hud: HUD = $HUD


func _ready():
	var room := _get_current_room()

	if room:
		_setup_room(room)


func _add_inventory_item(text: String, texture_path: String):
	Inventory.add(text, texture_path)
	hud.show_item_pickup(text, texture_path)


# Inform the user if no child of type Room is found.
func _get_configuration_warning() -> String:
	if not _get_current_room():
		return "No child of type \"Room\" found."

	return ""


# Search for the current room and return if found.
func _get_current_room() -> Room:
	for child in get_children():
		if child is Room:
			return child

	return null


# Set up room
func _setup_room(room: Room):
	hud.connect("unpaused", room, "unpause")
	room.connect("on_item_collected", self, "_add_inventory_item")
	room.connect("paused", hud, "pause")
