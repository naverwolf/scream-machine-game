class_name Room
extends Node2D
# A segment of the game map in which objects and interactivity is contained.


signal on_item_collected(text, texture_path)
signal paused

var is_paused := true
var navigation_object: Interactable

onready var clicker: Area = $Clicker
onready var navigation: Navigation2D = $Navigation
onready var player: Player = $Player


func _ready():
	for child in $Interactables.get_children():
		child.connect("clicked", self, "_on_Interactable_clicked", [child])

	player.is_paused = true


func _physics_process(_delta):
	if is_paused:
		return

	if Input.is_action_just_pressed("ui_cancel"):
		pause()


func pause(notify := true):
	is_paused = true
	player.is_paused = true
	pause_mode = PAUSE_MODE_STOP

	if notify:
		emit_signal("paused")


func unpause():
	is_paused = false
	player.is_paused = false
	pause_mode = PAUSE_MODE_INHERIT


func _navigate_to(vec2: Vector2, doubleclick: bool):
	var path := navigation.get_simple_path(player.position, vec2)

	# Remove the player's position
	path.remove(0)

	player.movement_path = path
	player.running = doubleclick


func _on_Clicker_clicked(vec2: Vector2, doubleclick: bool):
	navigation_object = null
	_navigate_to(vec2, doubleclick)


func _on_Interactable_clicked(_vec2: Vector2, doubleclick: bool, node: Interactable):
	navigation_object = node
	_navigate_to(node.position, doubleclick)


func _on_Player_movement_completed():
	if navigation_object:
		var data = navigation_object.use()

		if navigation_object is Item:
			pause(false)
			emit_signal("on_item_collected", data.text, data.texture_path)
