class_name Player
extends Entity
# The player character, controlled by the user.


signal movement_completed

const SPEED_RUNNING := 1500.0
const SPEED_WALKING := 750.0

var movement_path: PoolVector2Array = []
var running := false


func _process_movement(delta: float):
	# Do nothing unless we have a movement path
	if movement_path.size() == 0:
		return

	var goal: Vector2 = movement_path[0]
	var speed: float = SPEED_RUNNING if running else SPEED_WALKING

	var distance := max(goal.distance_to(position) - speed * delta, 0.0)

	position = goal + goal.direction_to(position) * distance

	if position == goal:
		movement_path.remove(0)

		if movement_path.size() == 0:
			emit_signal("movement_completed")


func _update(delta: float):
	_process_movement(delta)
