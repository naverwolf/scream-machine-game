class_name Curtain
extends ColorRect
# A black sheet to cover the screen for transitions.


signal faded

onready var tween: Tween = $Tween


func fade_to(value: float, time := 1.0):
	tween.interpolate_property(self, "color:a", color.a, value, time)
	tween.start()


func _on_Tween_tween_completed(_object, _key):
	emit_signal("faded")


func _on_Tween_tween_step(_object, _key, _elapsed, _value):
	visible = color.a != 0
