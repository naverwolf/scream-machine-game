class_name InventoryBar
extends Control
# Display of the current inventory, also allowing for using items.


var selected_item: InventoryItem = null

onready var bar: Control = $Bar
onready var item_title: ItemTitle = $Bar/ItemTitle
onready var items: Array = $Bar/InventoryItems.get_children()
onready var toggle: Button = $Bar/Toggle
onready var toggle_tween: Tween = $Bar/Toggle/Tween


func _ready():
	Inventory.connect("modified", self, "update_data")
	update_data()

	for item in items:
		item.connect("pressed", self, "_on_InventoryItem_pressed", [item])
		item.connect("set_title", self, "_set_item_title")


func update_data():
	var data := Inventory.get_all()

	for i in range(0, items.size()):
		var item: InventoryItem = items[i]

		if i < data.size():
			item.setup(data[i].text, data[i].texture_path)
		else:
			item.clear()


func _on_InventoryItem_pressed(item: InventoryItem):
	if item.title == "":
		selected_item = null
		_set_item_title("")
		return

	if not selected_item:
		selected_item = item
		_set_item_title("")
		return

	if selected_item == item:
		selected_item = null
		_set_item_title("")
		return

	Inventory.combine(item.title, selected_item.title)
	selected_item = null
	return


func _on_Toggle_toggled(on: bool):
	toggle_tween.stop_all()
	var goal: float

	if on:
		goal = -240
		toggle.text = "Hide Items"
	else:
		goal = 0
		toggle.text = "Show Items"

	toggle_tween.interpolate_property(bar, "rect_position:y", bar.rect_position.y, goal, .25)
	toggle_tween.start()


func _set_item_title(text: String):
	if selected_item:
		item_title.text = "Use %s on" % selected_item.title

		if text:
			text = "Use %s on %s" % [selected_item.title, text]

		return

	item_title.text = "" if text == "" else "Use %s" % text
