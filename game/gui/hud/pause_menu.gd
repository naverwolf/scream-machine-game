class_name PauseMenu
extends VBoxContainer
# Menu to be shown when game is paused.


signal unpaused


func _process(_delta: float):
	if not visible:
		return

	if Input.is_action_just_pressed("ui_cancel"):
		emit_signal("unpaused")
