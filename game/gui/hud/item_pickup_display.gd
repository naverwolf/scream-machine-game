class_name ItemPickupDisplay
extends VBoxContainer
# Display to the user the item they just picked up.


signal begin_fading_out
signal faded

onready var label: Label = $Label
onready var texture_rect: TextureRect = $TextureRect
onready var tween: Tween = $Tween


func display(item: String, texture_path: String):
	show()
	label.text = item
	texture_rect.texture = load(texture_path)

	modulate.a = 0
	fade(1, .5)
	yield(self, "faded")
	yield(get_tree().create_timer(1), "timeout")
	fade(0, .5)
	emit_signal("begin_fading_out")
	yield(self, "faded")

	hide()


func fade(value: float, time := 1.0):
	tween.interpolate_property(self, "modulate:a", modulate.a, value, time)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("faded")
