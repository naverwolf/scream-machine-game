class_name HUD
extends CanvasLayer
# Heads up display shown during gameplay.


signal curtain_faded
signal unpaused

onready var curtain: Curtain = $Curtain
onready var item_pickup_display: ItemPickupDisplay = $ItemPickupDisplay
onready var pause_menu: PauseMenu = $PauseMenu


func _ready():
	item_pickup_display.hide()
	pause_menu.hide()

	curtain.fade_to(0.0, 1)
	yield(curtain, "faded")
	emit_signal("unpaused")


func pause():
	curtain.fade_to(.25, .5)
	yield(curtain, "faded")
	pause_menu.show()


func show_item_pickup(item: String, texture_path: String):
	curtain.fade_to(.5, .5)
	item_pickup_display.display(item, texture_path)
	yield(item_pickup_display, "begin_fading_out")
	curtain.fade_to(0.0, .5)
	emit_signal("unpaused")


func _on_Curtain_faded():
	emit_signal("curtain_faded")


func _on_PauseMenu_unpaused():
	pause_menu.hide()
	curtain.fade_to(0.0, .5)
	yield(curtain, "faded")
	emit_signal("unpaused")
