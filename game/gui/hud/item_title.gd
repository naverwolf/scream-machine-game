class_name ItemTitle
extends ColorRect
# Displays the title of the currently highlighted item.


const FADE_TIME := 0.15

var text setget _set_text,_get_text

onready var label: Label = $Label
onready var tween: Tween = $Tween


func _ready():
	modulate.a = 0


func _get_text() -> String:
	return label.text


func _set_text(_text: String):
	tween.stop_all()

	var goal: float
	var time: float

	# Setting a blank text fades it out, setting a text fades in and sets string
	if _text == "":
		goal = 0.0
		time = modulate.a * FADE_TIME
	else:
		goal = 1.0
		time = (1.0 - modulate.a) * FADE_TIME
		label.text = _text

	tween.interpolate_property(self, "modulate:a", modulate.a, goal, time)
	tween.start()
