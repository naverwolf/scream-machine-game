class_name Interactable
extends Area2D
# Base class for different kinds of interactable element.


signal clicked(vec2, doubleclick)

var title setget ,_get_title

var _highlighted := false

onready var label: Label = $Label


func _ready():
	label.hide()


func _unhandled_input(event: InputEvent):
	# Do nothing if releasing mouse button instead of pressing or not button
	if not (event is InputEventMouseButton) or not event.pressed or not _highlighted:
		return

	get_tree().set_input_as_handled()
	emit_signal("clicked", get_global_mouse_position(), event.doubleclick)


func use(_item = null):
	pass # Virtual method


func _get_title() -> String:
	return label.text


func _on_Interactable_mouse_entered():
	_highlighted = true
	label.show()


func _on_Interactable_mouse_exited():
	_highlighted = false
	label.hide()
