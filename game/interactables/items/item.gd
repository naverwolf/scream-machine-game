class_name Item
extends "res://interactables/interactable.gd"
# Item to be picked up and used by the player.


onready var sprite: Sprite = $Sprite


func use(_item = null):
	call_deferred("queue_free")
	return {
		"text": _get_title(),
		"texture_path": sprite.texture.resource_path
	}
